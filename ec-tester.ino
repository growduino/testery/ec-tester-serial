/*****************************************************

Sketch to measure CO2 concentration using MHZ19 sensor
and display it on 20x4 lcd.

BSD license 

*****************************************************/
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27,20,4); 

#include "GrowduinoFirmware3.h"
#include "serial_sensors.h"

float result;
int loop_counter;

void lcd_print(String msg) {
  Serial.println(msg);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(msg);
}

void lcd_print_low(String msg) {
  Serial.println(msg);
  lcd.setCursor(0,1);
  lcd.print(msg);
}

void setup() {
  Serial.begin(115200);
  Serial.println("PH Sensor Test (measure serial version)");
  lcd.init();
  // Print a message to the LCD.
  lcd_print("hello, world!");

  lcd.setBacklight(HIGH);
}

void loop() {
  loop_counter++;
  result = getECMeasure_serial();

  String line1 = String("EC: ") + String(result);
  String line2 = String( loop_counter % 10 );
  lcd_print(line1);
  lcd_print_low(line2);
  Serial.println(line1);
  delay(1000);
}

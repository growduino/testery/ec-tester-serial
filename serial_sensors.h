#pragma once

#include <SoftwareSerial.h>

void send_serial_ident(SoftwareSerial serial);
float measure_serial(SoftwareSerial serial, int sleep_ater_m);

float getPARMeasure_serial();
float getPAR2Measure_serial();
float getECMeasure_serial();

#include "GrowduinoFirmware3.h"

#include <SoftwareSerial.h>

SoftwareSerial ECSerial(EC_RX, EC_TX);
SoftwareSerial PHSerial(PH_RX, PH_TX);
SoftwareSerial PARSerial(PAR1_RX, PAR1_TX);
SoftwareSerial PAR2Serial(PAR2_RX, PAR2_TX);

#include "util.h"
/* new (serial) version */
#define BUFLEN 32

char ecbuf[BUFLEN + 1]; //+1 for \0
char ec_c, char_read;
int ecbuf_idx;
long unsigned int ec_val, ec_triple;
int serial_buffer_idx;
unsigned long sensor_value, sensor_value_triple;
char serial_buffer[BUFLEN + 1]; //+1 for \0
char read_char;


void send_serial_ident(SoftwareSerial serial) {
  serial.flush();
  delay(100);

  serial.print("ident ");
  delay(100);
  int buffer_index = 0;
  strcpy(serial_buffer, "                            ");
  if (serial.available()) {
    while ((serial.available()) && (buffer_index < BUFLEN)) {
      read_char = serial.read();
      serial_buffer[buffer_index] = read_char;
      buffer_index++;
      delay(3);
    }
    serial_buffer[buffer_index] = 0;
  }
  Serial.print("D: ident ");
  Serial.println(serial_buffer);
}

float measure_serial(SoftwareSerial serial, int sleep_ater_m) {
  Serial.println("D: measure_serial");
  int buffer_index = 0;

  serial.flush();

  serial.print("m ");
  long unsigned int sensor_value = -1;
  long unsigned int sensor_value_triple = -1;
  strcpy(serial_buffer, "                            ");

  int delay_time = 10;
  int max_delay = sleep_ater_m / delay_time;
  int delay_ctr = 0;

  while (!serial.available() && delay_ctr++ < max_delay) {
    delay(delay_time);
  }

  if (serial.available()) {
    while ((serial.available()) && (buffer_index < BUFLEN)) {
      read_char = serial.read();
      serial_buffer[buffer_index] = read_char;
      buffer_index++;
      delay(3);
    }
    serial_buffer[buffer_index] = 0;
    Serial.print("D: serial_buffer ");
    Serial.println(serial_buffer);
    sscanf(serial_buffer, "%lu,%lu", &sensor_value, &sensor_value_triple);

    if (((sensor_value * 3 % 1000000) == sensor_value_triple) &&
        sensor_value > 0) {
      return float(sensor_value);
    }
  } else {
    Serial.print("D: softserial no response after [s] ");
    Serial.println(sleep_ater_m / 1000.0);
  }
  return MINVALUE;
}


float measure_ph() {
  pinMode(PH_RX, INPUT_PULLUP);
  pinMode(PH_TX, OUTPUT);
  PHSerial.begin(19200);
  PHSerial.listen();
  send_serial_ident(PHSerial);
  float sensor_value = measure_serial(PHSerial, 5000);
  PHSerial.end();
  if ((!isinf(sensor_value)) && (sensor_value > 0) && (sensor_value < 16368)) {
    float ret = -1.5 + float(sensor_value) / 963;
    return ret;
  }
  return MINVALUE;
}

float curr_to_ppfd(float current){
  Serial.print("D: Curr in: ");
  Serial.println(current);
  float k = 2.558;
  float f = 0.97;
  float ppfd = (current / 96.6057) * k * f;
  Serial.print("D: ppfd out: ");
  Serial.println(ppfd);

  return ppfd;
}

float getECMeasure_serial() {
  Serial.print("D: ec at: RX: ");
  Serial.print(EC_RX);
  Serial.print(" TX: ");
  Serial.println(EC_TX);
  pinMode(EC_RX, INPUT_PULLUP);
  pinMode(EC_TX, OUTPUT);
  ECSerial.begin(19200);
  ECSerial.listen();
  float sensor_value = measure_serial(ECSerial, 1500);
  ECSerial.end();
  if (!isinf(sensor_value)) {
    return sensor_value;
  }
  return MINVALUE;
}


float getPARMeasure_serial() {
  pinMode(PAR1_RX, INPUT);
  pinMode(PAR1_TX, OUTPUT);

  PARSerial.begin(19200);
  PARSerial.listen();
  float sensor_value = measure_serial(PARSerial, 500);
  PARSerial.end();
  if (!isinf(sensor_value)) {
    return curr_to_ppfd(float(sensor_value));
  }
  return MINVALUE;
}

float getPAR2Measure_serial() {
  pinMode(PAR2_RX, INPUT);
  pinMode(PAR2_TX, OUTPUT);

  PAR2Serial.begin(19200);
  PAR2Serial.listen();
  float sensor_value = measure_serial(PAR2Serial, 500);
  PAR2Serial.end();
  if (!isinf(sensor_value)) {
    return curr_to_ppfd(float(sensor_value));
  }
  return MINVALUE;
}
